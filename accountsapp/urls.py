from django.urls import path
from rest_framework.urlpatterns import format_suffix_patterns
from accountsapp import views

urlpatterns = [

    path('accounts/', views.SnippetList.as_view()),
    path('accounts/<int:pk>/', views.SnippetDetail.as_view()),
    # path('users/', views.UserList.as_view()),
    # path('users/<pk>/', views.UserDetails.as_view()),
    # path('groups/', views.GroupList.as_view()),
]

urlpatterns = format_suffix_patterns(urlpatterns)
